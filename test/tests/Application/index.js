const assert = require('assert');
const path = require('path');

const application = require('../../../src/Application/index').instance;

describe('tests for Application class', () => {
    describe('tests for version getter', () => {
        it('successfully worked', () => {
            let version = require('../../../package').version;

            assert.strictEqual(application.version, version);
        });
    });

    describe('tests for basePath function', () => {
        it('that the return value of the basePath function is correct', function () {
            let dir = path.join(__dirname, '../..');

            assert.strictEqual(application.basePath(), dir)
            assert.strictEqual(application.basePath('bootstrap'), path.join(dir, 'bootstrap'))
        });
    });

    describe('tests for _getClassType function', () => {
        it('should return "unknown"', () => {
            assert.strictEqual(application._getClassType(1), 'unknown');
            assert.strictEqual(application._getClassType(new Date()), 'unknown');
            assert.strictEqual(application._getClassType([1, 2, 3]), 'unknown');
        });

        it('should return "provider"', function () {
            assert.strictEqual(application._getClassType('route'), 'provider');
        });

        it('should return "alias"', function () {
            assert.strictEqual(application._getClassType('Route'), 'alias');
        });

        it('should return "autoLoad"', function () {
            assert.strictEqual(application._getClassType('App.Http.Controller'), 'autoLoad');
        });
    });

    describe('tests for _getCurrentDirectory function', () => {
        it('should return mocha path', () => {
            assert.ok(
                application._getCurrentDirectory()
                    .endsWith(path.normalize('apricot/core/node_modules/mocha/lib'))
            )
        });
    });

    describe('tests for _getDirectoryForFrameworkClass function', () => {
        it('should return path contents this "env/src/Env/EnvProvider"', () => {
            assert.ok(
                application._getDirectoryForFrameworkClass('Apricot/Env/EnvProvider')
                    .endsWith(path.normalize('env/src/Env/EnvProvider'))
            );
        });

        it('should return path contents this "core/src/Helpers/index"', () => {
            assert.ok(
                application._getDirectoryForFrameworkClass('Apricot/Helpers')
                    .endsWith(path.normalize('core/src/Helpers/index'))
            );
        });
    });

    describe('tests for use function', () => {
		it('provider should return 1', function () {
            assert.equal(use('route'), 1);
		});

		it('alias should return 1', function () {
            assert.equal(use('Route'), 1);
		});

		it('auto load should return 1', function () {
            assert.equal(use('App/Test').test, 1);
		});

		it('unknown should return empty string', function () {
            assert.equal(use(1), '');
		});
	});

    describe('tests for namespaceToPath function', () => {
		it('should return path contents this "env/src/Env/EnvProvider"', () => {
			assert.ok(
				application.namespaceToPath('Apricot/Env/EnvProvider')
					.endsWith(path.normalize('env/src/Env/EnvProvider'))
			);
		});

		it('should return path contents this "core/src/Helpers/index"', () => {
			assert.ok(
				application.namespaceToPath('Apricot/Helpers')
					.endsWith(path.normalize('core/src/Helpers/index'))
			);
		});

		it('should return path contents this "resource/Models/Users"', () => {
			assert.ok(
				application.namespaceToPath('Resource/Models/Users')
					.endsWith(path.normalize('Resource/Models/Users'))
			);
		});

		it('should return path contents this "app/Providers/Route"', () => {
			assert.ok(
				application.namespaceToPath('App/Providers/Route')
					.endsWith(path.normalize('app/Providers/Route'))
			);
		});

		it('should return path contents this "database/seeds/User"', () => {
			assert.ok(
				application.namespaceToPath('User')
					.endsWith(path.normalize('database/seeds/User'))
			);
		});

		it('should return path contents this "Current"', () => {
			assert.ok(
				application.namespaceToPath('Current')
					.endsWith('Current')
			);
		});
	});

    describe('tests for _resolveProvider function', () => {
		it('should return 1', function () {
			assert.equal(application._resolveProvider(application._providers.get('route')), 1);
		});
	});

    describe('tests for _bind function', () => {
		it('should insert provider to list', function () {
		    application._bind('testing', () => {});
            assert.ok(application._providers.has('testing'));
		});

		it('should fail', () => {
			assert.throws(
			    () => application._bind('testing', ''),
                Error,
                'Invalid closure, bind expects a callback'
            );
		});
	});

	describe('tests for singleton function', () => {
		it('provider should be singleton type insert in list', function () {
			application.singleton('testing', () => {});
			assert.ok(application._providers.get('testing').singleton);
		});
	});

	describe('tests for bind function', () => {
		it('provider should be not singleton type insert in list', function () {
			application.bind('testing', () => {});
			assert.ok(!application._providers.get('testing').singleton);
		});
	});

	describe('tests for _autoLoad function', () => {
		it('should return 1', function () {
			assert.equal(use('App/Test').test, 1);
		});
	});

	describe('tests for _resolveAlias function', () => {
		it('should return 1', function () {
            assert.equal(application._resolveAlias('App/Facade/Route'), 1);
		});
	});

	describe('test for make function', () => {
		it('provider should return 1', function () {
            assert.equal(application.make('route'), 1);
		});

		it('alias should return 1', function () {
            assert.equal(application.make('Route'), 1);
		});

		it('json should return "testing"', function () {
            assert.equal(application.make('App/Config').env, 'testing');
		});

		it('class should return "testing"', function () {
            assert.equal(application.make('App/Test').test(), 'testing');
		});

		// it('class with inject should return "testing"', function () {
		//     let inject = application.make('App/Inject');
		//     console.log(inject.test(), inject.testing)
         //    // assert.equal(inject.test(), 'testing');
         //    // assert.equal(inject.test, 'testing');
		// });
	});
});