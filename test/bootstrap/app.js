module.exports = {
    providers: [
        'App/Providers/Route'
    ],

    aliases: {
        Route: 'App/Facade/Route'
    }
};