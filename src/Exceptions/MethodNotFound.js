/**
 * @class MethodNotFound
 * @memberOf Apricot.Application
 */
class MethodNotFound extends Error {
    /**
     * @param {object} instance
     * @param {string} method
     */
    constructor(instance, method) {
        super(`This ${method} does not exists on the ${instance} instance`);

        this._instance = instance;
        this._method = method;
    }
}

module.exports = MethodNotFound;