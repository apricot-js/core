/**
 * @class InvalidNamespace
 * @memberOf Apricot.Application
 */
class InvalidNamespace extends Error {
    /**
     * @param {string} namespace
     */
    constructor(namespace) {
        super('Invalid namespace for require specify file or class');

        this._namespace = namespace;
    }
}

module.exports = InvalidNamespace;