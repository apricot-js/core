const path = require('path');
const fs = require('fs');

const InvalidArgumentException = require('../Exceptions/InvalidArgument');
const InvalidNamespaceException = require('../Exceptions/InvalidNamespace');
const MethodNotFoundException = require('../Exceptions/MethodNotFound');
const CyclicInjectionException = require('../Exceptions/CyclicInjection');

/**
 * @typedef {Object} provider
 * @property {function} closure
 * @property {boolean} singleton
 * @property {*} instance
 */

/**
 * @class Application
 * @memberOf Apricot.Application
 */
class Application {
    /**
     * Get Application instance
     *
     * @returns {Application}
     */
    static get instance() {
        if (!Application._instance) {
            Application._instance = new Application();
        }

        return Application._instance;
    }

    /**
     * get Apricot JS framework classes prefix
     *
     * @returns {string}
     */
    get frameworkNamespace() {
        return 'Apricot';
    }

    /**
     * Get Apricot JS framework core version
     *
     * @returns {string}
     */
    get version() {
        let json = require(path.join(__dirname, '..', '..', 'package.json'));

        return json && json.version ? json.version : '0';
    }

    /**
     * Get providers list
     *
     * @returns {array<string>}
     */
    get providers() {
        if (!this._providerCache) {
            let providers = new Set([...this._bootstarp.providers]);
            this._providerCache = Array.from(providers);
        }

        return this._providerCache
    }

	/**
     * Return modules which it is in core package
     *
	 * @return {string[]}
	 */
	get coreModules() {
        return ['Application', 'Helpers', 'Support', 'Reflection'];
    }

    constructor() {
        this._bootstarp = require(path.join(this.basePath(), 'bootstrap', 'app'));

        /**
         * Provider list
         * @type {Map.<string, provider>}
         * @private
         */
        this._providers = new Map();

        this._setGlobalVariable();
        this.use('Apricot.Helpers');
        this._register();
    }

    /**
     * Get normalize path for framework folder
     * 
     * @param {string} [dir] 
     * @returns {string}
     */
    frameworkPath (dir) {
        return path.normalize(
            dir ?
                path.join(__dirname, '../../..', dir) :
                path.join(__dirname, '../../..')
        );
    }

    /**
     * Get normalize path for project folder
     *
     * @param {string} [dir]
     * @returns {string}
     */
    basePath(dir) {
        return path.normalize(
            dir ?
                path.join(process.cwd(), dir) :
                process.cwd()
        );
    }

    /**
     * Get normalize path for application folder
     *
     * @param {string} [dir]
     * @returns {string}
     */
    appPath(dir) {
        return path.normalize(
            dir ?
                path.join(this.basePath(), 'app', dir) :
                path.join(this.basePath(), 'app')
        );
    }

    /**
     * Get normalize path for data base folder
     *
     * @param {string} [dir]
     * @returns {string}
     */
    databasePath(dir) {
        return path.normalize(
            dir ?
                path.join(this.basePath(), 'database', dir) :
                path.join(this.basePath(), 'database')
        );
    }

    /**
     * Get normalize path for resource folder
     *
     * @param {string} [dir]
     * @returns {string}
     */
    resourcePath(dir) {
        return path.normalize(
            dir ?
                path.join(this.basePath(), 'resources', dir) :
                path.join(this.basePath(), 'resources')
        );
    }

    /**
     * Get normalize path for storage folder
     *
     * @param {string} [dir]
     * @returns {string}
     */
    storagePath(dir) {
        return path.normalize(
            dir ?
                path.join(this.basePath(), 'storage', dir) :
                path.join(this.basePath(), 'storage')
        );
    }

    /**
     * Get normalize path for config folder
     *
     * @param {string} [dir]
     * @returns {string}
     */
    configPath(dir) {
        return path.normalize(
            dir ?
                path.join(this.basePath(), 'config', dir) :
                path.join(this.basePath(), 'config')
        );
    }

    /**
     * Get normalize path for public folder
     *
     * @param {string} [dir]
     * @returns {string}
     */
    publicPath(dir) {
        return path.normalize(
            dir ?
                path.join(this.basePath(), 'public', dir) :
                path.join(this.basePath(), 'public')
        );
    }

    /**
     * Parse and require namespace specify file or class
     *
     * @param {string} namespace
     * @returns {*}
     */
    use(namespace) {
        let type = this._getClassType(namespace);

        switch (type) {
            case 'provider':
                return this._resolveProvider(this._providers.get(namespace));
            case 'autoLoad':
                return this._autoLoad(namespace);
            case 'alias':
                return this._resolveAlias(this._bootstarp.aliases[namespace]);
            default:
                return '';
        }
    }

    /**
     * Make class file real path
     *
     * @param {string} namespace
     * @returns {string}
     */
    namespaceToPath(namespace) {
        namespace = namespace.replace(/\./g, '/').replace(/\/js/, '.js');

        if (namespace.startsWith(this.frameworkNamespace)) {
            namespace = this._getDirectoryForFrameworkClass(namespace);
        } else {
            switch (true) {
                case fs.existsSync(this.basePath(`${namespace.lcfirst().replace('.js', '')}.js`)):
                    namespace = this.basePath(`${namespace.lcfirst().replace('.js', '')}.js`);
                    break;
                case fs.existsSync(this.databasePath(path.join('seeds', `${ namespace.replace('.js', '') }.js`))):
                    namespace = this.databasePath(path.join('seeds', `${ namespace.replace('.js', '') }.js`));
                    break;
                default:
                    namespace = path.join(this._getCurrentDirectory(), namespace);
                    break;
            }
        }

        return path.normalize(namespace).replace('.js', '');
    }

    /**
     * Register provide as singleton
     *
     * @param {string} namespace
     * @param {function} closure
     */
    singleton(namespace, closure) {
        this._bind(namespace, closure, true);
    }

    /**
     * Register provide as normal
     *
     * @param {string} namespace
     * @param {function} closure
     */
    bind(namespace, closure) {
        this._bind(namespace, closure);
    }

	make(namespace, injectionMap = new Map()) {
		let type = this._getClassType(namespace);
		let instance;

		if (type === 'provider' || type === 'alias') {
			return this.use(namespace)
		}

		if (type === 'autoLoad') {
			instance = this._autoLoad(namespace)
		}

		if (!this._isClass(instance)) {
			return instance;
		}

		if (!instance.inject || Object.keys(instance.inject).length === 0) {
			return new instance();
		}

		let injects = this._introspect(instance.toString());

		if (instance.inject.length < injects.length) {
		    let error = new InvalidArgumentException('Attributes count and injections property count not mach.');
		    error.argument = [instance.inject, injects];

		    throw error;
		}

		injectionMap.set(instance, instance.inject);

		const resolvedInjections = injects.map(inject => {
		    if (Object.values(injectionMap).indexOf(inject) > -1) {
		        throw new CyclicInjectionException(inject);
            }

            return this.make(instance.inject[inject], injectionMap);
        });

		return new (Function.prototype.bind.apply(instance, [null].concat(resolvedInjections)))();
	};

	/**
     * Parse string and make class instance and method
     *
	 * @param {string} binding
	 * @return {{instance: *, method: string}}
	 */
	makeFunction(binding) {
		let parts = binding.split('.');

		if (parts.length !== 2) {
		    let error = new InvalidArgumentException('Unable parse and make function');
		    error.argument = binding;

		    throw error;
		}

		let [instance, method] = parts;

		instance = this.make(instance);

		if (!instance[method]) {
			throw new MethodNotFoundException(instance.prototype.constructor.name, method)
		}

		return {instance, method};
	}

    /**
     * Set Global variable
     *
     * @private
     */
    _setGlobalVariable() {
        /**
         * @property { Application }
         * @name global#application
         */
        Object.defineProperty(
            global,
            'application',
            {
                value: this,
                configurable: false,
                writable: false,
                enumerable: false
            }
        );

        /**
         * @property { function }
         * @name global#use
         */
        Object.defineProperty(
            global,
            'use',
            {
                value: this.use.bind(this),
                configurable: false,
                writable: false,
                enumerable: false
            }
        );

        /**
         * @property { function }
         * @name global#basePath
         */
        Object.defineProperty(
            global,
            'basePath',
            {
                value: this.basePath.bind(this),
                configurable: false,
                writable: false,
                enumerable: false
            }
        );

        /**
         * @property { function }
         * @name global#appPath
         */
        Object.defineProperty(
            global,
            'appPath',
            {
                value: this.appPath.bind(this),
                configurable: false,
                writable: false,
                enumerable: false
            }
        );

        /**
         * @property { function }
         * @name global#configPath
         */
        Object.defineProperty(
            global,
            'configPath',
            {
                value: this.configPath.bind(this),
                configurable: false,
                writable: false,
                enumerable: false
            }
        );

        /**
         * @property { function }
         * @name global#databasePath
         */
        Object.defineProperty(
            global,
            'databasePath',
            {
                value: this.databasePath.bind(this),
                configurable: false,
                writable: false,
                enumerable: false
            }
        );

        /**
         * @property { function }
         * @name global#publicPath
         */
        Object.defineProperty(
            global,
            'publicPath',
            {
                value: this.publicPath.bind(this),
                configurable: false,
                writable: false,
                enumerable: false
            }
        );

        /**
         * @property { function }
         * @name global#resourcePath
         */
        Object.defineProperty(
            global,
            'resourcePath',
            {
                value: this.resourcePath.bind(this),
                configurable: false,
                writable: false,
                enumerable: false
            }
        );

        /**
         * @property { function }
         * @name global#storagePath
         */
        Object.defineProperty(
            global,
            'storagePath',
            {
                value: this.storagePath.bind(this),
                configurable: false,
                writable: false,
                enumerable: false
            }
        );
    }

    /**
     * Detect class type
     *
     * @param {string} namespace
     * @returns {string}
     * @private
     */
    _getClassType(namespace) {
        if (typeof namespace !== 'string') {
            return 'unknown';
        } else if (this._providers.has(namespace)) {
            return 'provider';
        } else if (this._bootstarp.aliases[namespace]) {
            return 'alias';
        } else {
            return 'autoLoad';
        }
    }

    /**
     * Register provider in application
     *
     * @private
     */
    _register() {
        this.providers
            .forEach(provider => {
                const providerClass = this.use(provider);

                if (providerClass === false) {
                    return;
                }

                const instance = new providerClass();

                instance.register();
                instance.boot();
            });
    }

    /**
     * Resolve provider by class name
     *
     * @param {provider} provider
     * @returns {*}
     * @private
     */
    _resolveProvider(provider) {
        if (!provider.singleton) {
            return provider.closure(this);
        }

        provider.instance = provider.instance || provider.closure(this);

        return provider.instance;
    }

    /**
     * Bind provider instance by class name
     *
     * @param {string} namespace
     * @param {function} closure
     * @param {boolean} singleton
     * @private
     * @throws InvalidArgumentException
     */
    _bind(namespace, closure, singleton = false) {
        if (typeof closure !== 'function') {
            let error = new InvalidArgumentException('Invalid closure, bind expects a callback');
            error.argument = closure;

            throw error;
        }

        namespace = namespace.trim();

        this._providers.set(namespace, { closure: closure, singleton: singleton, instance: null });
    }

    /**
     * Require class by namespace
     *
     * @param {string} namespace
     * @returns {*}
     * @private
     */
    _autoLoad(namespace) {
        let original = namespace;

        namespace = this.namespaceToPath(namespace);

        try {
            const classObject = require(namespace);
            classObject.namespace = original;

            return classObject;
        } catch (e) {
            throw e;
        }
    }

    /**
     * Detect current directory path
     *
     * @returns {string}
     * @private
     */
    _getCurrentDirectory() {
        let stacks = (new Error()).stack.split(/\n\s+/);
        let match = stacks[5] ? stacks[5].match(/^at\s*([\w\\. <>]+)\s\((.+):(\d+):(\d+)\)$/) : [];

        return match && match.length ? path.normalize(path.dirname(match[2])) : '';
    }

    /**
     * Get framework class real path from namespace
     *
     * @param {string} namespace
     * @returns {string}
     * @private
     */
    _getDirectoryForFrameworkClass(namespace) {
        let match = namespace.match(/^([a-zA-Z]+)\/([a-zA-Z]+)\/?([a-zA-Z]*)/);

        if (!match) {
            throw new InvalidNamespaceException(namespace);
        }

        let [, , module, file] = match;
        let group = this.coreModules.indexOf(module) > -1 ? 'core' : module.toLowerCase();

        return path.join(this.frameworkPath(), group, 'src', module, file === '' ? 'index' : file);
    }

    /**
     * Resolve provider by alias
     *
     * @param {string} namespace
     * @returns {*}
     * @private
     */
    _resolveAlias(namespace) {
        /**
         * @type {{facadeAccessor}}
         */
        let instance = this._autoLoad(namespace);

        return typeof instance.facadeAccessor === 'string' ?
            this.use(instance.facadeAccessor) :
            instance.facadeAccessor;
    }

	/**
     * Check object it is class
     *
	 * @param {*} binding
	 * @return {boolean}
	 * @private
	 */
	_isClass(binding) {
		return typeof binding === 'function' && typeof binding.constructor === 'function' && !!binding.name;
	}

	/**
     * Analyse class instance and detect injections property
     *
	 * @param definition
	 * @return {*}
	 * @private
	 */
	_introspect(definition) {
		let args = /(constructor|^function)\s*\w*\(([\s\S]*?)\)/.exec(definition);

		if (!args || !args[2]) {
			return [];
		}

		args = args[2].trim();

		if (args.length === 0) {
			return [];
		}

		return args.split(/[ ,\n\r\t]+/).map(argument => argument.replace(/_/g, '/'));
	}
}

module.exports = Application;