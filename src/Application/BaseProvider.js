/**
 * @class BaseProvider
 * @memberOf Apricot.Application
 */
class BaseProvider {
    constructor() {
        this.app = application;
    }

    boot() {

    }

    register() {

    }
}

module.exports = BaseProvider;